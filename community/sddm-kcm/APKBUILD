# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sddm-kcm
pkgver=5.23.2
pkgrel=0
pkgdesc="Config module for SDDM"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://www.kde.org"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-only"
depends="
	sddm
	systemsettings
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kxmlgui-dev
	libxcursor-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	xcb-util-image-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
af078750f1a8cd9b34c955984f2c0dc63a74d9c576cf4d601ca2b0b70e6a68760c11ded10ee9f79217cd9d2d6575a664c3767cf5f1f486f4210e6b4bb0a675e1  sddm-kcm-5.23.2.tar.xz
"
