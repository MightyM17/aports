# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-gtk
pkgver=5.23.2
pkgrel=0
pkgdesc="A GTK Theme Built to Match KDE's Breeze"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> breeze
arch="noarch !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only"
depends="gtk-engines"
makedepends="
	breeze
	breeze-dev
	extra-cmake-modules
	py3-cairo
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-gtk-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
a064b757d34e7ee247d974225021ba95d07830f3fd519be4ff3c30c3bf583559510d5f9013425aaed48307474930244d2e0c75b5139d2cc45e144d47f6552967  breeze-gtk-5.23.2.tar.xz
"
